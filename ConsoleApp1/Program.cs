﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Fraction a = new Fraction(1,2);
            Fraction b = new Fraction(4,6);
            Console.WriteLine($"Перший дріб = {a}");
            Console.WriteLine($"Другий дріб = {b}");
            Console.WriteLine($"Бінарна операція a + b {a + b}");
            Console.WriteLine($"Бінарна операція a - b {a - b}");
            Console.WriteLine($"Бінарна операція a * b {a * b}");
            Console.WriteLine($"Бінарна операція a / b  {b / a}");
            Console.WriteLine($"Унарна операція +a {+a}"); a = new Fraction(1, 2);
            Console.WriteLine($"Унарна операція -a {-a}"); a = new Fraction(1, 2);
            Console.WriteLine($"Унарна операція ++a {++a}"); a = new Fraction(1, 2);
            Console.WriteLine($"Унарна операція --a {--a}"); a = new Fraction(1, 2);
            Console.WriteLine($"Унарна операція +b {+b}"); b = new Fraction(4, 6);
            Console.WriteLine($"Унарна операція -b {-b}"); b = new Fraction(4, 6);
            Console.WriteLine($"Унарна операція ++b {++b}"); b = new Fraction(4, 6);
            Console.WriteLine($"Унарна операція --b {--b}"); b = new Fraction(4, 6);
            Console.WriteLine($"Операція порівняння a > b {a > b}");
            Console.WriteLine($"Операція порівняння a < b {a < b}");
            Console.WriteLine($"Операція порівняння a >= b {a >= b}");
            Console.WriteLine($"Операція порівняння a <= b {a <= b}");
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
    }
}
